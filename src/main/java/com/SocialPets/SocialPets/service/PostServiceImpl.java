package com.SocialPets.SocialPets.service;

import com.SocialPets.SocialPets.dto.PostDTO;
import com.SocialPets.SocialPets.dto.UserDTO;
import com.SocialPets.SocialPets.model.Comment;
import com.SocialPets.SocialPets.model.Post;
import com.SocialPets.SocialPets.model.User;
import com.SocialPets.SocialPets.repository.CommentRepository;
import com.SocialPets.SocialPets.repository.PostRepository;
import com.SocialPets.SocialPets.repository.UserRepository;
import com.SocialPets.SocialPets.util.CommentMapper;
import com.SocialPets.SocialPets.util.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class PostServiceImpl implements PostService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public PostDTO findPostById(String id) {
        Optional<Post> post = postRepository.findById(id);
        if(post.isPresent()){
            Post found = post.get();
            return new PostDTO(found.getId(),
                    UserMapper.mapUserToUserDTO(found.getUser(),false),
                    found.getText(),
                    found.getImgPath(),
                    found.getDate(),
                    CommentMapper.mapSetComments(found.getComments()));
        }
        return null;
    }

    @Override
    public String writePost(PostDTO dto, String id){
        Post post = new Post();

        Optional<User> user = userRepository.findById(id);

        if(user.isPresent()){
            post.setText(dto.getText())
                    .setImgPath(dto.getImgPath())
                    .setUser(user.get())
                    .setDate(dto.getDate());
            return postRepository.save(post).getId();
        } else{
            return null;
        }
    }

    @Override
    public List<PostDTO> getPosts(UserDTO user, int page) {

        List<PostDTO> results = new ArrayList<>();

        if(user.getFollowing() != null){
            List<String> ids = user.getFollowing().stream()
                    .map(UserDTO::getId)
                    .collect(Collectors.toList());
            ids.add(user.getId());

            Optional<List<User>> users = Optional.ofNullable(userRepository.findAllByIdIn(ids));
            if(users.isPresent()){
                Optional<List<Post>> posts = Optional.ofNullable(postRepository.findAllByUserIn(users.get(),
                        (Pageable) PageRequest.of(page, 6, Sort.Direction.DESC, "Date")));
                if(posts.isPresent()){
                    for (Post post : posts.get()
                         ) {
                        results.add(new PostDTO(post.getId(),
                                UserMapper.mapUserToUserDTO(post.getUser(), false),
                                post.getText(),
                                post.getImgPath(),
                                post.getDate(),
                                CommentMapper.mapSetComments(post.getComments())));
                    }
                }
            }
        }
        return results;
    }

    @Override
    public void deletePost(String id, String userEmail) {
        Optional<Post> dbpost = postRepository.findById(id);
        if(dbpost.isPresent()){
            Post post = dbpost.get();

            if(post.getUser().getEmail().equals(userEmail)){
                if(post.getComments() != null){
                    commentRepository.deleteAll(post.getComments().stream()
                            .filter(x -> x != null)
                            .collect(Collectors.toList()));
                }
                postRepository.delete(post);
            }
        }
    }

}
