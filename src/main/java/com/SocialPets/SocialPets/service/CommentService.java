package com.SocialPets.SocialPets.service;

import com.SocialPets.SocialPets.dto.CommentDTO;

public interface CommentService {

    String writeComment(CommentDTO dto, String postID, String userEmail);

    CommentDTO findById(String id);

    void deleteComment(String commentId, String postId, String userEmail);
}
