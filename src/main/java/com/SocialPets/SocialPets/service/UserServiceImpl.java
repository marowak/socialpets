package com.SocialPets.SocialPets.service;

import com.SocialPets.SocialPets.dto.UserDTO;
import com.SocialPets.SocialPets.model.User;
import com.SocialPets.SocialPets.repository.UserRepository;
import com.SocialPets.SocialPets.util.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Component
public class UserServiceImpl implements UserService {

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @Override
    public void signup(UserDTO userDto) {
        User user = new User().setEmail(userDto.getEmail())
                .setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()))
                .setName(userDto.getName());
        userRepository.save(user);
    }

    @Override
    public UserDTO findUserById(String id, boolean getFollowers) {
        Optional<User> user = userRepository.findById(id);
        return user.map(value -> UserMapper.mapUserToUserDTO(value, getFollowers)).orElse(null);
    }

    public UserDTO findUserByEmail(String email) {
        Optional<User> user = Optional.ofNullable(userRepository.findByEmail(email));
        return user.map(value -> UserMapper.mapUserToUserDTO(value, true)).orElse(null);
    }

    @Override
    public void updateProfile(UserDTO userDto) {
        Optional<User> user = Optional.ofNullable(userRepository.findByEmail(userDto.getEmail()));
        if (user.isPresent()) {
            User userModel = user.get();
            userModel.setName(userDto.getEmail())
                    .setName(userDto.getName())
                    .setPhoneNumber(userDto.getPhoneNumber())
                    .setDescription(userDto.getDescription());
            if(!userDto.getImgPath().isEmpty()){
                userModel.setImgPath(userDto.getImgPath());
            }
            userRepository.save(userModel);
        }
    }

    @Override
    public void changePassword(UserDTO userDto, String newPassword) throws Exception {
        Optional<User> user = Optional.ofNullable(userRepository.findByEmail(userDto.getEmail()));
        if (user.isPresent()) {
            User userModel = user.get();
            if(bCryptPasswordEncoder.matches(userDto.getPassword(), userModel.getPassword())){
                userModel.setPassword(bCryptPasswordEncoder.encode(newPassword));
                userRepository.save(userModel);
            } else {
                throw new Exception("Wrong password");
            }
        }
    }

    @Override
    public void AddFollower(String userEmail, String id ) {

        Optional<User> followedUSer = userRepository.findById(id);
        Optional<User> user = Optional.ofNullable(userRepository.findByEmail(userEmail));

        if (user.isPresent() && followedUSer.isPresent()) {
            User userModel = user.get();
            if(userModel.getFollowing() == null){
                userModel.setFollowing(new HashSet<>());
            }
            userModel.getFollowing().add(followedUSer.get());
            userRepository.save(userModel);
        }
    }

    @Override
    public void RemoveFollower(String userEmail, String id ) {

        Optional<User> user = Optional.ofNullable(userRepository.findByEmail(userEmail));

        if (user.isPresent()) {
            User userModel = user.get();
            if(userModel.getFollowing() != null){
                userModel.getFollowing().removeIf(obj -> obj.getId().equals(id));
                userRepository.save(userModel);
            }
        }
    }

    @Override
    public List<UserDTO> findAllByNameRegex(String name){
        Optional<List<User>> users = Optional.ofNullable(userRepository.findAllByNameRegex(".*" + name  + ".*"));
        List<UserDTO> lstDTO = new ArrayList<>();
        if (users.isPresent()) {
            for (User user : users.get()
            ) {
                UserDTO dto = UserMapper.mapUserToUserDTO(user, false);
                lstDTO.add(dto);
            }
        }
        return lstDTO;
    }

    @Override
    public List<UserDTO> findAllByIdIn(List<UserDTO> dtos) {

        List<String> ids = new ArrayList<>();

        for (UserDTO dto : dtos
             ) {
            ids.add(dto.getId());
        }

        Optional<List<User>> users = Optional.ofNullable(userRepository.findAllByIdIn(ids));
        List<UserDTO> lstDTO = new ArrayList<>();

        if (users.isPresent()) {
            for (User user : users.get()
            ) {
                UserDTO dto = UserMapper.mapUserToUserDTO(user, false);
                lstDTO.add(dto);
            }
        }

        return lstDTO;

    }


}
