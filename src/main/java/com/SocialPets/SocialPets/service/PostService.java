package com.SocialPets.SocialPets.service;

import com.SocialPets.SocialPets.dto.PostDTO;
import com.SocialPets.SocialPets.dto.UserDTO;

import java.util.List;

public interface PostService {

    String writePost(PostDTO dto, String id);

    PostDTO findPostById(String id);

    List<PostDTO> getPosts(UserDTO user, int page);

    void deletePost(String id,  String userEmail);

}
