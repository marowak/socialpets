package com.SocialPets.SocialPets.service;

import com.SocialPets.SocialPets.dto.CommentDTO;
import com.SocialPets.SocialPets.model.Comment;
import com.SocialPets.SocialPets.model.Post;
import com.SocialPets.SocialPets.model.User;
import com.SocialPets.SocialPets.repository.CommentRepository;
import com.SocialPets.SocialPets.repository.PostRepository;
import com.SocialPets.SocialPets.repository.UserRepository;
import com.SocialPets.SocialPets.util.CommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;

@Component
public class CommentServiceImpl implements CommentService {


    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public String writeComment(CommentDTO dto, String postID, String userEmail) {

        Optional<User> dbuser = Optional.ofNullable(userRepository.findByEmail(userEmail));
        Optional<Post> dbpost = postRepository.findById(postID);

        if(dbpost.isPresent() && dbuser.isPresent()){
            Post post = dbpost.get();
            User user = dbuser.get();
            if(post.getComments() == null){
                post.setComments(new HashSet<Comment>());
            }
            Comment comment = new Comment();
            comment.setUser(user);
            comment.setText(dto.getText());
            comment.setDate(LocalDateTime.now());

            post.getComments().add(comment);

            Comment saved = commentRepository.save(comment);
            postRepository.save(post);
            return saved.getId();
        }

        return null;

    }

    @Override
    public CommentDTO findById(String id) {
        Optional<Comment> dbcomment = commentRepository.findById(id);
        if(dbcomment.isPresent()){
            Comment comment = dbcomment.get();
            return CommentMapper.mapComment(comment);
        }
        return null;
    }

    @Override
    public void deleteComment(String commentId, String postId, String userEmail) {
        Optional<Comment> dbcomment = commentRepository.findById(commentId);
        if(dbcomment.isPresent()){
            Comment comment = dbcomment.get();
            if(comment.getUser().getEmail().equals(userEmail)){
                Optional<Post> dbPost = postRepository.findById(postId);
                if(dbPost.isPresent()){
                    Post post = dbPost.get();
                    if(post.getComments() != null){
                        post.getComments().removeIf(com -> com.getId().equals(commentId));;
                        postRepository.save(post);
                        commentRepository.delete(comment);
                    }
                }
            }
        }
    }
}
