package com.SocialPets.SocialPets.service;

import com.SocialPets.SocialPets.dto.UserDTO;

import java.util.List;

public interface UserService {

    void signup(UserDTO userDto);

    UserDTO findUserById(String email, boolean getFollowers);

    UserDTO findUserByEmail(String email);

    void updateProfile(UserDTO userDto);

    void changePassword(UserDTO userDto, String newPassword) throws Exception;

    void AddFollower(String email, String addedId);

    void RemoveFollower(String email, String addedId);

    List<UserDTO> findAllByNameRegex(String name);

    List<UserDTO> findAllByIdIn(List<UserDTO> dtos);

}
