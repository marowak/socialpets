package com.SocialPets.SocialPets.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class Post {

    @Id
    private String id;

    @DBRef
    private User user;

    private String text;

    private String imgPath;

    private LocalDateTime Date;

    @DBRef
    private Set<Comment> comments;

}
