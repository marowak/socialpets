package com.SocialPets.SocialPets.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ChangePswRequest {


    @NotBlank
    private String password;

    @NotBlank
    private String newPassword;

}
