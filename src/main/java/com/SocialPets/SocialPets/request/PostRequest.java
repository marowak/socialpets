package com.SocialPets.SocialPets.request;

import lombok.Data;

@Data
public class PostRequest {
    String text;
    String imgPath;
    String userId;
}
