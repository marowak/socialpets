package com.SocialPets.SocialPets.request;

import lombok.Data;

@Data
public class CommentRequest {
    String text;
    String postID;
}
