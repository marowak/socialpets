package com.SocialPets.SocialPets.util;

import com.SocialPets.SocialPets.dto.UserDTO;
import com.SocialPets.SocialPets.model.User;

import java.util.HashSet;

public class UserMapper {

    public static UserDTO mapUserToUserDTO(User user, boolean getFollowers){

        HashSet<UserDTO> following = new HashSet<>();

        //Não precisa sempre pegar os followers
        if(getFollowers && user.getFollowing() != null){
            for (User usr : user.getFollowing()
            ) {
                following.add(new UserDTO(usr.getId(), usr.getEmail(), usr.getPassword(), usr.getName(),
                        usr.getPhoneNumber(), usr.getDescription(), usr.getImgPath(),
                        null));
            }
        }

        return new UserDTO(user.getId(),
                user.getEmail(),
                user.getPassword(),
                user.getName(),
                user.getPhoneNumber(),
                user.getDescription(),
                user.getImgPath(),
                following);
    }

}
