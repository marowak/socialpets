package com.SocialPets.SocialPets.util;

import com.SocialPets.SocialPets.dto.CommentDTO;
import com.SocialPets.SocialPets.model.Comment;

import java.util.HashSet;
import java.util.Set;

public class CommentMapper {

    public static CommentDTO mapComment(Comment comment){
        return new CommentDTO(comment.getId(),
                UserMapper.mapUserToUserDTO(comment.getUser(),false),
                comment.getText(),
                comment.getDate());
    }

    public static Set<CommentDTO> mapSetComments(Set<Comment> comments){

        Set<CommentDTO> mapped = new HashSet<>();
        if(comments != null) {
            for (Comment comment : comments
                 ) {
                if(comment != null){
                    mapped.add(new CommentDTO(comment.getId(),
                            UserMapper.mapUserToUserDTO(comment.getUser(),false),
                            comment.getText(),
                            comment.getDate()));
                }
            }
        }
        return mapped;
    }

}
