package com.SocialPets.SocialPets.repository;

import com.SocialPets.SocialPets.model.Post;
import com.SocialPets.SocialPets.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.data.domain.Pageable;
import java.util.List;

public interface PostRepository extends MongoRepository<Post, String> {

    List<Post> findAllByUserIn(List<User> users, Pageable pageable);

}
