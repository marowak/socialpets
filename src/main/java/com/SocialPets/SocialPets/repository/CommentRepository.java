package com.SocialPets.SocialPets.repository;

import com.SocialPets.SocialPets.model.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CommentRepository extends MongoRepository<Comment, String> {

}
