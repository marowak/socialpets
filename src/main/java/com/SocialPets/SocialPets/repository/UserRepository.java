package com.SocialPets.SocialPets.repository;

import com.SocialPets.SocialPets.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {

    User findByEmail(String email);

    List<User> findAllByNameRegex(String name);

    List<User> findAllByIdIn(List<String> ids);

}
