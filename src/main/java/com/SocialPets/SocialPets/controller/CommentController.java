package com.SocialPets.SocialPets.controller;

import com.SocialPets.SocialPets.dto.CommentDTO;
import com.SocialPets.SocialPets.request.CommentRequest;
import com.SocialPets.SocialPets.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
public class CommentController {

    @Autowired
    CommentService commentService;

    @PostMapping(value="/writeComment")
    @ResponseBody
    public String writeComment(@RequestBody CommentRequest request, Model model, Principal principal){

        CommentDTO dto = new CommentDTO();
        dto.setText(request.getText());
        return commentService.writeComment(dto, request.getPostID(), principal.getName());

    }

    @GetMapping(value="/loadComment")
    public String loadComment(@RequestParam String id, @RequestParam String postId, Model model, Principal principal){
        CommentDTO dto = commentService.findById(id);
        if(dto != null){
            model.addAttribute("comment", dto);
            model.addAttribute("postId", postId);
            model.addAttribute("userEmail", principal.getName());
            return "fragments/comments :: comments";
        }
        return "";
    }

    @GetMapping(value="/deleteComment")
    @ResponseBody
    public String deletePost(@RequestParam String idComment, @RequestParam String idPost, Principal principal){

        commentService.deleteComment(idComment, idPost,principal.getName());

        return "success";
    }


}
