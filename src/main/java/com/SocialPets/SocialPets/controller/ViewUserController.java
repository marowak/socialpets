package com.SocialPets.SocialPets.controller;

import com.SocialPets.SocialPets.dto.UserDTO;
import com.SocialPets.SocialPets.request.SearchUserRequest;
import com.SocialPets.SocialPets.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class ViewUserController {

    @Autowired
    UserService userService;

    @GetMapping(value = {"/viewUser"})
    public ModelAndView searchUserView(@RequestParam String id, Principal principal) {
        ModelAndView modelAndView = new ModelAndView("viewUser");

        UserDTO dto = userService.findUserByEmail(principal.getName());

        if(id.equals(dto.getId())){
            modelAndView.setViewName("home");
        } else {
            UserDTO viewedDto = userService.findUserById(id, false);
            modelAndView.addObject("usr", viewedDto);
            //Olha se algum dos followers tem o mesmo e-mail do usuário vendo o perfil para exibir o botão de follow/unfollow
            boolean isFollowing = false;

            if(dto.getFollowing() != null){
                isFollowing = dto.getFollowing().
                        stream().anyMatch(follower -> follower.getId().equals(id));
            }
            modelAndView.addObject("isFollowing", isFollowing);
        }

        return modelAndView;
    }

    @GetMapping(value = {"/followUser"})
    @ResponseBody
    public String followUser(@RequestParam String id, Principal principal) {

        userService.AddFollower(principal.getName(), id);
        return "Success";
    }

    @GetMapping(value = {"/unfollowUser"})
    @ResponseBody
    public String unfollowUser(@RequestParam String id, Principal principal) {

        userService.RemoveFollower(principal.getName(), id);
        return "Success";
    }

    @PostMapping(value={"/followUser"})
    public ModelAndView doSearchUser(@Valid @ModelAttribute("SearchUserRequest") SearchUserRequest searchUserRequest) {

        ModelAndView modelAndView = new ModelAndView("searchUser");

        try {
            modelAndView.addObject("listUsers",userService.findAllByNameRegex(searchUserRequest.getNome()));

        } catch (Exception exception) {
            modelAndView.addObject("error", "Falha ao atualizar registro!");
        }

        return modelAndView;
    }

}
