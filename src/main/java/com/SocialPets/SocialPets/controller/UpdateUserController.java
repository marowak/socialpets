package com.SocialPets.SocialPets.controller;

import com.SocialPets.SocialPets.dto.UserDTO;
import com.SocialPets.SocialPets.request.UpdateUserRequest;
import com.SocialPets.SocialPets.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.io.File;
import java.security.Principal;

@Controller
public class UpdateUserController {

    @Value("${images.path}")
    private String path;

    @Autowired
    private UserService userService;

    @GetMapping(value = {"/updateUser"})
    public ModelAndView updateUser(Principal principal) {

        ModelAndView modelAndView = new ModelAndView("updateUser");
        UserDTO dto = userService.findUserByEmail(principal.getName());

        UpdateUserRequest request = new UpdateUserRequest();

        request.setNome(dto.getName());
        request.setEmail(dto.getEmail());
        request.setDescription(dto.getDescription());
        request.setPhoneNumber(dto.getPhoneNumber());

        modelAndView.addObject("UpdateUserRequest", request);

        return modelAndView;
    }

    @PostMapping(value={"/updateUser"})
    public ModelAndView doUpdate(@Valid @ModelAttribute("UpdateUserRequest") UpdateUserRequest updateUserRequest, BindingResult bindingResult, Principal principal) {
        ModelAndView modelAndView = new ModelAndView("updateUser");

        if (bindingResult.hasErrors()) {
            modelAndView.addObject("error", "Os campos de e-mail e telefone são obrigatórios!");
        } else {
            try {

                String filePath = "";

                if(!updateUserRequest.getImagem().isEmpty()){

                    UserDTO currentUser = userService.findUserByEmail(principal.getName());
                    filePath = path + currentUser.getId();
                    if(! new File(filePath).exists())
                    {
                        new File(filePath).mkdir();
                    }

                    String orgName = updateUserRequest.getImagem().getOriginalFilename();
                    filePath = filePath  + '\\' + orgName;
                    File dest = new File(filePath);
                    updateUserRequest.getImagem().transferTo(dest);

                    filePath = filePath.split("static")[1];
                }

                UserDTO dto = new UserDTO();
                dto.setName(updateUserRequest.getNome());
                dto.setEmail(updateUserRequest.getEmail());
                dto.setPhoneNumber(updateUserRequest.getPhoneNumber());
                dto.setDescription(updateUserRequest.getDescription());
                dto.setImgPath(filePath);
                userService.updateProfile(dto);
            } catch (Exception exception) {
                modelAndView.addObject("error", "Falha ao atualizar registro!");
            }
        }
        return modelAndView;
    }
}
