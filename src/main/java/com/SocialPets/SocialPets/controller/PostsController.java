package com.SocialPets.SocialPets.controller;

import com.SocialPets.SocialPets.dto.CommentDTO;
import com.SocialPets.SocialPets.dto.PostDTO;
import com.SocialPets.SocialPets.dto.UserDTO;
import com.SocialPets.SocialPets.request.PostRequest;
import com.SocialPets.SocialPets.service.PostService;
import com.SocialPets.SocialPets.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class PostsController {

    @Autowired
    UserService userService;

    @Autowired
    PostService postService;

    @PostMapping(value="/writePost")
    @ResponseBody
    public String writePost(@RequestBody PostRequest request, Principal principal){

        PostDTO dto = new PostDTO();
        dto.setImgPath(request.getImgPath());
        dto.setText(request.getText());
        dto.setDate(java.time.LocalDateTime.now());

        String postId = postService.writePost(dto, request.getUserId());

        if(postId != null) {
            return postId;
        } else {
            return "";
        }
    }

    @GetMapping(value="/loadPost")
    public String loadPost(@RequestParam String id, Model model, Principal principal){
        PostDTO dto = postService.findPostById(id);
        if(dto != null){
            ArrayList<PostDTO> response = new ArrayList<PostDTO>();
            response.add(dto);
            model.addAttribute("posts", response);
            model.addAttribute("userEmail", principal.getName());
        }
        return "fragments/posts :: posts";
    }

    @GetMapping(value="/posts")
    public String getPosts(Model model, @RequestParam String id, @RequestParam Integer page, Principal principal){
        UserDTO user;
        if(id.isEmpty()){
            user = userService.findUserByEmail(principal.getName());
        } else {
            user = userService.findUserById(id, true);
            //Se está vendo o perfil de outro usuário, vê apenas os posts do próprio usuário e não quem ele segue
            if(!user.getEmail().equals(principal.getName())){
                user.setFollowing(Collections.<UserDTO>emptySet());
            }
        }


        if(user != null){
            List<PostDTO> response = postService.getPosts(user,page);
            for (PostDTO dto : response
                 ) {
                if(dto.getComments()!= null){
                    dto.setComments(dto.getComments().stream().sorted(Comparator.comparing(CommentDTO::getDate).reversed()).collect(Collectors.toCollection(LinkedHashSet::new)));
                }
            }
            model.addAttribute("userEmail", principal.getName());
            model.addAttribute("posts", response );
        }

        return "fragments/posts :: posts";
    }

    @GetMapping(value="deletePost")
    @ResponseBody
    public String deletePost(@RequestParam String id, Principal principal){

        postService.deletePost(id, principal.getName());

        return "success";
    }

}
