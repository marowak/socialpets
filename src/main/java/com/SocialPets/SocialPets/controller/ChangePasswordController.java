package com.SocialPets.SocialPets.controller;

import com.SocialPets.SocialPets.dto.UserDTO;
import com.SocialPets.SocialPets.request.ChangePswRequest;
import com.SocialPets.SocialPets.request.UpdateUserRequest;
import com.SocialPets.SocialPets.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class ChangePasswordController {
    @Autowired
    UserService userService;

    @GetMapping(value = {"/changePassword"})
    public ModelAndView changePassword(Principal principal) {

        ModelAndView modelAndView = new ModelAndView("changePassword");
        ChangePswRequest request = new ChangePswRequest();
        modelAndView.addObject("ChangePswRequest", request);

        return modelAndView;
    }

    @PostMapping(value={"/changePassword"})
    public ModelAndView doUpdate(@Valid @ModelAttribute("ChangePswRequest") ChangePswRequest request, BindingResult bindingResult, Principal principal) {
        ModelAndView modelAndView = new ModelAndView("changePassword");
        if (bindingResult.hasErrors()) {
            modelAndView.addObject("error", "Falha ao atualizar senha!");
        } else {
            try {
                UserDTO dto = new UserDTO();
                dto.setEmail(principal.getName());
                dto.setPassword(request.getPassword());
                userService.changePassword(dto, request.getNewPassword());

            } catch (Exception exception) {
                modelAndView.addObject("error", "Senha atual incorreta!");
            }
        }
        return modelAndView;
    }
}
