package com.SocialPets.SocialPets.controller;

import com.SocialPets.SocialPets.dto.UserDTO;
import com.SocialPets.SocialPets.request.SignUpRequest;
import com.SocialPets.SocialPets.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class LoginController {

    @Autowired
    UserService userService;

    @GetMapping("/")
    public String redirectLogin() {
        return "redirect:home";
    }

    @RequestMapping(value={"/login"}, method = {RequestMethod.GET})
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("SignUpRequest", new SignUpRequest());
        return modelAndView;
    }

    @GetMapping(value = {"/logout"})
    public String logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return "redirect:login";
    }

    @PostMapping(value={"{/login"})
    public ModelAndView loginPost(){
        return new ModelAndView("login");
    }

    @PostMapping(value={"/signup"})
    public ModelAndView signup(@Valid @ModelAttribute("SignUpRequest") SignUpRequest request, BindingResult bindingResult, HttpServletRequest httpRequest) {
        ModelAndView modelAndView = new ModelAndView("login");
        if (bindingResult.hasErrors()) {
            modelAndView.addObject("error", "E-mail inválido!");
        } else {

            if(userService.findUserByEmail(request.getEmail()) != null)
            {
                modelAndView.addObject("error", "E-mail já cadastrado!");
                return modelAndView;
            }
            try {
                UserDTO dto = new UserDTO();
                dto.setName(request.getNome());
                dto.setEmail(request.getEmail());
                dto.setPassword(request.getSenha());
                userService.signup(dto);
                httpRequest.login(dto.getEmail(), dto.getPassword());
                modelAndView.setViewName("home");
            } catch (Exception exception) {
                modelAndView.addObject("error", "Falha ao criar novo registro!");
            }
        }
        return modelAndView;

    }
}
