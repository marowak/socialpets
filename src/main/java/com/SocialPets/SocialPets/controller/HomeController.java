package com.SocialPets.SocialPets.controller;

import com.SocialPets.SocialPets.service.PostService;
import com.SocialPets.SocialPets.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

@Controller
public class HomeController {

    @Autowired
    UserService userService;

    @Autowired
    PostService postService;

    @GetMapping(value="home")
    public String homePage(){
        return "home";
    }

    @PostMapping(value="home")
    public String home(){return "redirect:home";}

    @GetMapping(value="/home/loadUser")
    public String loadUser(Model model, Principal principal){
        model.addAttribute("usr", userService.findUserByEmail(principal.getName()));
        return "fragments/profile :: profile";
    }

}
