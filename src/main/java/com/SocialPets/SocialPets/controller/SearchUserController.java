package com.SocialPets.SocialPets.controller;

import com.SocialPets.SocialPets.request.SearchUserRequest;
import com.SocialPets.SocialPets.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class SearchUserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = {"/searchUser"})
    public ModelAndView searchUserView() {
        ModelAndView modelAndView = new ModelAndView("searchUser");

        SearchUserRequest request = new SearchUserRequest();

        modelAndView.addObject("SearchUserRequest", request);
        modelAndView.addObject("listUsers", null);

        return modelAndView;
    }

    @PostMapping(value={"/searchUser"})
    public ModelAndView doSearchUser(@Valid @ModelAttribute("SearchUserRequest") SearchUserRequest searchUserRequest) {

        ModelAndView modelAndView = new ModelAndView("searchUser");

        try {
            modelAndView.addObject("listUsers",userService.findAllByNameRegex(searchUserRequest.getNome()));

        } catch (Exception exception) {
            modelAndView.addObject("error", "Falha ao atualizar registro!");
        }

        return modelAndView;
    }

}
