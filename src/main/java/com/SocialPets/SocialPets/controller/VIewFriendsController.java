package com.SocialPets.SocialPets.controller;

import com.SocialPets.SocialPets.dto.UserDTO;
import com.SocialPets.SocialPets.request.SearchUserRequest;
import com.SocialPets.SocialPets.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
public class VIewFriendsController {

    @Autowired
    private UserService userService;

    @GetMapping(value = {"/viewFriends"})
    public ModelAndView viewFriends(Principal principal) {
        ModelAndView modelAndView = new ModelAndView("viewFriends");

        UserDTO dto = userService.findUserByEmail(principal.getName());
        if(dto.getFollowing() != null){
            modelAndView.addObject("listUsers", dto.getFollowing());
        }

        return modelAndView;
    }
}
