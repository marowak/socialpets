package com.SocialPets.SocialPets.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

    private String Id;
    private String email;
    private String password;
    private String name;
    private String phoneNumber;
    private String description;
    private String imgPath;
    private Set<UserDTO> following;
}
