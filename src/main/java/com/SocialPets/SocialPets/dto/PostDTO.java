package com.SocialPets.SocialPets.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostDTO {

    private String id;

    private UserDTO user;

    private String text;

    private String imgPath;

    private LocalDateTime Date;

    private Set<CommentDTO> comments;

}
