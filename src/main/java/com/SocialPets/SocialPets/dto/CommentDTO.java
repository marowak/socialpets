package com.SocialPets.SocialPets.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentDTO {

    private String id;

    private UserDTO user;

    private String text;

    private LocalDateTime Date;

}
