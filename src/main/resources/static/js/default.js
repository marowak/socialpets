page = 0;

$( document ).ready(function() {

    //Auto resize pra textareas crescerem com o texto
    $('textarea').each(function () {
      this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
    }).on('input', function () {
      this.style.height = 'auto';
      this.style.height = (this.scrollHeight) + 'px';
    });

    var debounced = _.debounce(function() {
                           if($(".profile-scroll").scrollTop() + $(".profile-scroll").height() > $(".profile-scroll").prop('scrollHeight') - 100) {
                                loadPosts();
                           }
                        } , 250);

    $(".profile-scroll").scroll(debounced);
    loadUsr();
    loadPosts();
});
//Funções de fragments/profile ========================================================================================


function loadUsr() {

    if($('#resultsBlock').length == 0){
        return false;
    }
    var url = '/home/loadUser';
    $("#resultsBlock").load(url);
}

//Funções de viewUser =================================================================================================
function followUser() {

    var url = "/followUser?id=" + $('#userId').val();

    $.ajax({
        type : "GET",
        url : url,
        success : function(response) {
            swapButtons();
        },
        error : function(e) {
            $.toast({
                heading: 'Erro',
                text: 'Falha ao seguir usuário.',
                position: 'top-right',
                stack: false
            });
        }
    });
}

function unfollowUser() {

    var url = "/unfollowUser?id=" + $('#userId').val();

    $.ajax({
        type : "GET",
        url : url,
        success : function(response) {
            swapButtons();
        },
        error : function(e) {
            $.toast({
                heading: 'Erro',
                text: 'Falha ao completar sua requisição.',
                position: 'top-right',
                stack: false
            });
        }
    });
}

function swapButtons(){
    $(".follow").each(function(){
        var btn = $(this);
        if(btn.hasClass('d-none')){
            btn.removeClass('d-none');
        } else{
            btn.addClass('d-none');
        }
    });
}

//Funções de fragments/posts =================================================================================================
function loadPosts() {

    var div = $('.posts');
    if(div.length == 0){
        return false;
    }
    var userId = "";

    var usr = $("#userId").val();
    if(usr != null ){
        userId = usr;
    }

    //Remove class from the existing div that will receive the posts load
    div.removeClass('posts');
    var url = '/posts?id=' + userId + "&page=" + page;
    div.load(url, function(){
        var qtd = div.find('.postCard').length;
        page += 1;
        if(qtd == 6)
        {
            //Add a div that will receive the next batch when necessary
            div.after("<div class='posts'></div>");
        }
    });
}

function writePost() {

    var url = "/writePost";
    var postText = $("#postText").val();

    if(postText.length == 0){
        return false;
    }

    var imgPath = "";
    var userId = $("#userId").val();

    var post = {text:postText, imgPath:imgPath, userId:userId};

    $.ajax({
        type : "POST",
        contentType : "application/json",
        url : url,
        data : JSON.stringify(post),
        success : function(response) {
            $("#top-bar-posts").after("<div class='newPost'></div>");
            var newPost = $('.newPost');
            newPost.removeClass('newPost');
            newPost.load("/loadPost?id=" + response);
            $("#postText").val("");
        },
        error : function(e) {
            $.toast({
                heading: 'Erro',
                text: 'Falha ao fazer postagem.',
                position: 'top-right',
                stack: false
            });
        }
    });
}


function deletePost(id, e){

    var conf = !confirm("Tem certeza que deseja deletar a postagem?")
    if(id.length == 0 && conf){
        return false;
    }
    var url = "/deletePost?id=" + id;

    $.ajax({
        type : "GET",
        contentType : "application/json",
        url : url,
        ev : $(e.currentTarget),
        success : function(response) {
            this.ev.closest('.postCard').remove();
        },
        error : function(e) {
            $.toast({
                heading: 'Erro',
                text: 'Falha ao deletar postagem.',
                position: 'top-right',
                stack: false
            });
        }
    });
}


// COMMENTS ==============================

function collapsePost(e){
  var $_target =  $(e.currentTarget);
  var dataTarget = $_target.attr("data-target");
  if(dataTarget.length > 0){
    $_target.siblings(dataTarget).collapse('toggle');
  }
}

function writeComment(e){
    var url = "/writeComment";

    var textArea =  $($(e.currentTarget).parent().siblings('.comment-text')[0]);
    var targetDiv = $($(e.currentTarget).parent().siblings('.comments')[0]);

    if(textArea.val().length == 0){
        return false;
    }

    var comment = {text:textArea.val(), postID:$(e.currentTarget).attr("id")};

    $.ajax({
            type : "POST",
            contentType : "application/json",
            url : url,
            targetDiv : targetDiv,
            textArea : textArea,
            postID : $(e.currentTarget).attr("id"),
            data : JSON.stringify(comment),

            success : function(response) {
                if(this.targetDiv.children().length == 0){
                    this.targetDiv.append("<div class='row mt-3 commentRow'></div>");
                } else {
                    $(this.targetDiv.children()[0]).before("<div class='row mt-3 commentRow'></div>");
                }
                $(this.targetDiv.children()[0]).load("/loadComment?id=" + response + '&postId=' + this.postID);
                textArea.val("");
            },
            error : function(e) {
                $.toast({
                    heading: 'Erro',
                    text: 'Falha ao fazer comentário.',
                    position: 'top-right',
                    stack: false
                });
            }
        });
}

function deleteComment(idComment, idPost, e){

    var conf = !confirm("Tem certeza que deseja deletar o comentário?")
    if(idComment.length == 0 && idPost.length == 00 && conf){
        return false;
    }
    var url = "/deleteComment?idComment=" + idComment + '&idPost=' +idPost;

    $.ajax({
        type : "GET",
        contentType : "application/json",
        url : url,
        ev : $(e.currentTarget),
        success : function(response) {
            this.ev.closest('.commentRow').remove();
        },
        error : function(e) {
            $.toast({
                heading: 'Erro',
                text: 'Falha ao deletar comentário.',
                position: 'top-right',
                stack: false
            });
        }
    });
}
